// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.microdata_scraping;

import static org.openstreetmap.josm.tools.I18n.tr;

import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.MainMenu;
import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;

public class microdata_scrapingPlugin extends Plugin {

    MicroDataAction microdataAction;
    /**
     * Will be invoked by JOSM to bootstrap the plugin
     *
     * @param info  information about the plugin and its local installation
     */
    public microdata_scrapingPlugin(PluginInformation info) {
        super(info);
        // init your plugin
        microdataAction = new MicroDataAction();
        MainMenu.add(MainApplication.getMenu().toolsMenu, microdataAction);
    }

    @Override
    public void mapFrameInitialized(MapFrame oldFrame, MapFrame newFrame) {
    }
}
