package org.openstreetmap.josm.plugins.microdata_scraping;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

// Only used to test the scraper

public class MicroDataScraper {
    public static void main(String[] args) {
        String template = "no_template";
        String website = "no_website";
        Dictionary<String, String> tagDict = null;

        System.out.println("Microdata Scraper - Standalone utility");
        System.out.println("======================================");
        System.out.println("Arguments: " + Arrays.toString(args));

        if (args.length >= 1) {
            website = args[0];
            if (args.length >= 2) {
                template = args[1];
            }
            ScraperUtil scraperUtil = new ScraperUtil();
            tagDict = scraperUtil.scrap(website, template);
        } else {
            System.out.println("One should give at least one argument : the website");
            return;
        }
        System.out.println("Printing scraped tags:");
        System.out.println("----------------------");

        List<String> list = Collections.list(tagDict.keys());
        Collections.sort(list);

        for (Enumeration<String> e = Collections.enumeration(list); e.hasMoreElements();) {
            String key = e.nextElement();
            if (!StringUtils.isEmpty(tagDict.get(key))){
                System.out.println(key + " - " + tagDict.get(key));
            }
        }
        System.out.println("                      ");
        System.out.println("                      ");
    }
}

