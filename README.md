# Microdata scraping - JOSM plugin

Scraping microdata from various websites to convert them in openstreetmap tags.
The intended use is for food establishment websites but the scraping can be used on other sources.

The scraping works for the following protocol/websites :
- Microdata (schema.org)
- centralApp websites

The implementation is not complete and the plugin is in an early development stage. For now, the following tags can scraped if they are present as microdata in the source:

|OSM tags|||
|---|---|---|
|name            |airconditioned     |payment                  |
|addr:street     |bicycle_parking    |payment:american_express |
|addr:city       |catering           |payment:cash             |
|addr:country    |cuisine              |payment:visa             |
|addr:postcode   |lunch              |payment:mastercard       |
|contact:phone   |opening_hours      |payment:debit_cards      |
|contact:email   |outdoor_seating    |wheelchair     |
|image           |parking            |wifi      |

This is very handy for tags like `opening_hours` and `payment` as the OSM formatting is done by the plugin.
But note that a lot of websites do not provide any microdata.


## How to use

Once the plugin is [installed](https://josm.openstreetmap.de/wiki/Help/Preferences/Plugins#Manuallyinstallingplugins) in JOSM, 
one can select a node, a way or a relation and add the tag `website` with the website url as value.
The scraping can then be launched using Crtl+Maj+A shortcut or using the menu.

If microdata are found, they are automatically added to the object with metadata about the scraping.
Those metadata are set under the tags `scraper_microdata:*`.
By default, the scraper only adds tags that are not already existing. This behaviour can be changed by setting the tag `scraper_microdata:update` to `true` before launching the scraping.

Note that the scrapping can only be initiated on one element at a time.

## How to test the scraper

To test the scraper without using JOSM, one can launch the following command using the jar file:

```bash
java -cp microdata_scraping.jar org.openstreetmap.josm.plugins.microdata_scraping.MicroDataScraper www.website.com
```

## Dependencies

This plugin uses a modified version of [Pickaxe](https://github.com/mautini/pickaxe) which is a tool to extract Schema.org structured data from HTML pages.
The repositiorty of the modified version can be found [here](https://gitlab.com/vucod/pickaxe).

The plugin also uses the [schemaorg-java library](https://github.com/google/schemaorg-java/).
The other libraries used can be found in the `lib/` directory.

## How to build

Follow the instructions from [JOSM](https://josm.openstreetmap.de/wiki/DevelopersGuide/DevelopingPlugins).
Add this repository under josm/plugins.
Add the content of `src/main/java/` from the [modified pickaxe repository](https://gitlab.com/vucod/pickaxe) into `josm/plugins/microdata_scraping/src/` or put the associated jar file in the `lib/` directory.
Run `ant` in `josm/plugins/microdata/`.

## Release

One can also directly use the [jar file](https://gitlab.com/vucod/microdata-scraping/-/releases).

## Examples

Examples of websites containing microdata on food establishments:

- Restaurant websites created by CentralApp
- Restaurant websites created by Takeaway
- Tripadvisor.fr
- yelp.fr
- Ubereats.com
- cityplug.be
- brusselslife.be


## Links

-   [schema.org](https://schema.org/)
-   This plugin is based on the JOSM plugin [Austria Address Helper](https://github.com/JOSM/austriaaddresshelper).
