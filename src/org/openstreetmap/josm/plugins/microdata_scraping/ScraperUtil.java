package org.openstreetmap.josm.plugins.microdata_scraping;

import com.github.mautini.pickaxe.Scraper;
import com.github.mautini.pickaxe.model.Entity;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.gson.stream.MalformedJsonException;
import com.google.gson.JsonSyntaxException;
import com.google.schemaorg.SchemaOrgTypeImpl;
import com.google.schemaorg.SchemaOrgType;
import com.google.schemaorg.ValueType;
import com.google.schemaorg.core.Thing;
import com.google.schemaorg.core.PostalAddress;
import com.google.schemaorg.core.LocalBusiness;
import com.google.schemaorg.core.FoodEstablishment;
import com.google.schemaorg.core.CoreConstants;
import com.google.schemaorg.core.datatype.DataType;
import com.google.schemaorg.core.datatype.Text;
import com.google.schemaorg.JsonLdSerializer;
import com.google.schemaorg.JsonLdSyntaxException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.IllegalArgumentException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.BooleanUtils;

public class ScraperUtil {

    public static Dictionary<String, String> scrap(String website, String template){
        boolean noExceptionThrown = false;
        Exception exception = null;
        Document doc = null;
        Dictionary<String, String> tagDict = new Hashtable<String, String>();

        try {
            doc = Jsoup.connect(website).get();
            Element body = doc.body();
            Elements infos = body.select("div.info");

            // Schema.org
            if (!StringUtils.isEmpty(body.text())) {
                Scraper scraper = new Scraper();
                List<Entity> entityList = scraper.extract(doc.toString());
                System.out.println("## Microdata scraper : List of entity");
                for (Entity entity : entityList) {
                    Thing thing = entity.getThing();
                    JsonLdSerializer serializer = new JsonLdSerializer(true);
                    String jsonLdStr = serializer.serialize(thing);
                    System.out.println(jsonLdStr);

                    if (
                            (thing.getFullTypeName() == "http://schema.org/LocalBusiness") ||
                            (thing.getFullTypeName() == "http://schema.org/FoodEstablishment") || 
                            (thing.getFullTypeName() == "http://schema.org/Restaurant") 
                        ){
                        LocalBusiness foodesta = (LocalBusiness) thing;

                        putIfNotEmpty(tagDict, "name", Extract(foodesta.getNameList(), false));
                        putIfNotEmpty(tagDict, "currency:to_modified", Extract(foodesta.getCurrenciesAcceptedList(), true));
                        putIfNotEmpty(tagDict, "image", Extract(foodesta.getImageList(), true));
                        putIfNotEmpty(tagDict, "opening_hours", Extract(foodesta.getOpeningHoursList(), true));
                        //putIfNotEmpty(tagDict, "priceRange", Extract(foodesta.getPriceRangeList(), true));
                        putIfNotEmpty(tagDict, "payment", Extract(foodesta.getPaymentAcceptedList(), true));
                        putIfNotEmpty(tagDict, "contact:phone", Extract(foodesta.getTelephoneList(), false));
                        putIfNotEmpty(tagDict, "contact:email", Extract(foodesta.getEmailList(), false));

                        putAddressesList(tagDict, foodesta.getAddressList());
                    }
                    if (
                            (thing.getFullTypeName() == "http://schema.org/FoodEstablishment") || 
                            (thing.getFullTypeName() == "http://schema.org/Restaurant") 
                       ){
                        FoodEstablishment foodesta = (FoodEstablishment) thing;

                        putIfNotEmpty(tagDict, "cuisine", Extract(foodesta.getServesCuisineList(), true));

                        //putAddressesList(tagDict, foodesta.getAddressList());
                    }
                }
            }
            // Central App source
            if (!StringUtils.isEmpty(infos.text())) {
                String address = infos.select("address.list").html();
                Elements hours_sec = infos.select("div.hours");
                Elements extra = infos.select("div.info__cols").select(".info__col--large");

                // Name
                String name = infos.select("address.list").select("strong").text();
                putIfNotEmpty(tagDict, "name", name);

                // Address
                if (address.split("<br>").length > 2){
                    putIfNotEmpty(tagDict, "addr:street", address.split("<br>")[1].trim());
                    putIfNotEmpty(tagDict, "addr:city", address.split("<br>")[2].trim());
                    putIfNotEmpty(tagDict, "addr:country", address.split("<br>")[3].split("</p>")[0].trim());
                }


                // Payment
                String paymentBlock = extra.select("li:has(.icon--info-payment_means)").text();
                if (!StringUtils.isEmpty(paymentBlock)){
                    if (paymentBlock.split(":").length > 1){
                        String paymentList = paymentBlock.split(":")[1].trim();
                        if (paymentList.contains("American Express")){
                            putIfNotEmpty(tagDict, "payment:american_express", "yes");
                        }
                        if (paymentList.contains("Cash")){
                            putIfNotEmpty(tagDict, "payment:cash", "yes");
                        }
                        if (paymentList.contains("Visa")){
                            putIfNotEmpty(tagDict, "payment:visa", "yes");
                        }
                        if (paymentList.contains("Mastercard")){
                            putIfNotEmpty(tagDict, "payment:mastercard", "yes");
                        }
                        if (
                                paymentList.contains("Debit Card") || 
                                paymentList.contains("Carte de debit") ||
                                paymentList.contains("Debet kaart")
                                
                            ){
                            putIfNotEmpty(tagDict, "payment:maestro", "yes");
                        }
                    }
                }

                // Lunch
                String meals_served = extra.select("li:has(.icon--info-meals_served)").text().split(":")[1];
                String lunch = String.valueOf(meals_served.contains("Lunch") || meals_served.contains("Déjeuner"));
                putIfNotEmpty(tagDict, "lunch", lunch);

                // Openings
                if (!StringUtils.isEmpty(hours_sec.text())) {
                    String[] list_hours = hours_sec.select("span.hours__slots").html().split("\\r?\\n");
                    String hours = ("Mo "+list_hours[0]+"; "+"Tu "+list_hours[1]+"; "
                     +"We "+list_hours[2]+"; "+"Th "+list_hours[3]+"; " +"Fr "+list_hours[4]+"; "
                     +"Sa "+list_hours[5]+"; " +"Su "+list_hours[6]).replace("—","-").replace("  "," ").replace(" <br>",",").replace(" </br>",",");
                    putIfNotEmpty(tagDict, "opening_hours", hours);
                }

                // Others
                putIfNotEmpty(tagDict, "contact:phone", infos.select("span:has(.icon--phone)").text().trim());
                putIfNotEmpty(tagDict, "contact:phone", infos.select("span:has(.icon--mobile)").text().trim());
                putIfNotEmpty(tagDict, "wifi", ExtractBool(extra, "i.icon--info-wifi"));
                putIfNotEmpty(tagDict, "parking",ExtractBool(extra,  "i.icon--info-private_parking"));
                putIfNotEmpty(tagDict, "bicycle_parking", ExtractBool(extra, "i.icon--info-bike_parking"));
                putIfNotEmpty(tagDict, "wheelchair", ExtractBool(extra, "i.icon--info-wheelchair_access"));
                putIfNotEmpty(tagDict, "catering", ExtractBool(extra, "i.icon--info-catering"));
                putIfNotEmpty(tagDict, "outdoor_seating", ExtractBool(extra, "i.icon--info-outdoor_seatingr"));
                putIfNotEmpty(tagDict, "airconditioned", ExtractBool(extra, "i.icon--info-aircon"));

            }

            // Openings Takeaway
            Elements openings = body.select("div.openingtimes");
            if (!StringUtils.isEmpty(openings.text())) {
                putIfNotEmpty(tagDict, "opening_hours", getOpenings(openings, ".time", "takeaway"));
            }

            // Openings Yelp
            Elements openings_yelp = body.select("table.hours-table");
            if (!StringUtils.isEmpty(openings_yelp.text())) {
                putIfNotEmpty(tagDict, "opening_hours", getOpenings(openings_yelp, "td:not(.extra)", "yelp"));
            }

            if (tagDict.isEmpty()) {
                tagDict.put("Exception", "empty");
            } else {
                // Date
                String pattern = "MM/dd/yyyy HH:mm:ss";
                DateFormat df = new SimpleDateFormat(pattern);
                Date today = new Date();
                String today_str = df.format(today);
                
                tagDict.put("scraper_microdata:date", today_str);
    
                // Note
                tagDict.put("note", "Build using the microdata scraper. You should check the tags before saving!");
                tagDict.put("Exception", "false");
            }

            noExceptionThrown = true;
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
            exception = e;
        } catch (MalformedJsonException e) {
            e.printStackTrace();
            exception = e;
        } catch (JsonLdSyntaxException e) {
            e.printStackTrace();
            exception = e;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            exception = e;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            exception = e;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            exception = e;
        } catch (IOException e) {
            e.printStackTrace();
            exception = e;
        } catch (NullPointerException e) {
            e.printStackTrace();
            exception = e;
        } finally {
            if (!noExceptionThrown) {
                if (exception == null) {
                    tagDict.put("Exception", "Unknown exception");
                } else {
                    tagDict.put("Exception", exception.toString());
                }
            }
        }
        return tagDict;
    }

    public static String Extract(ImmutableList<SchemaOrgType> listProp, boolean concat){
        String value = "";
        if (listProp.size() > 0) {
            if (listProp.size() == 1) {
                if (listProp.get(0) instanceof DataType) {
                    DataType text = (DataType) listProp.get(0);
                    value = text.getValue();
                }
            } else if (concat == true){
                for (SchemaOrgType item : listProp) {
                    if (item instanceof DataType) {
                        DataType text = (DataType) item;
                        value = value + text.getValue();
                    }
                }
            }
        }
        return value;
    }

    public static String ExtractBool(Elements extra , String selector){
        return !extra.select(selector).isEmpty() ? "yes" : "no";
    }


    public static void putIfNotEmpty(Dictionary<String, String> tagDict, String key, String value){
        if (!StringUtils.isEmpty(value)) {
            tagDict.put(key, value);
        }
    }

    public static String getOpenings(Elements elements, String selector, String mode){
        List<String> list_hours = new ArrayList<String>();
        Elements elementsHours = elements.select(selector);
        for (Element e: elementsHours) {
            String hour = "none";
            if (mode.equals("takeaway")){
                hour = e.html().replace("<br>", ",");
            } else {
                hour = e.text();
            }
            list_hours.add(hour);
        }
        String hours = (
                "Mo "+list_hours.get(0)+"; "+
                "Tu "+list_hours.get(1)+"; "+
                "We "+list_hours.get(2)+"; "+
                "Th "+list_hours.get(3)+"; "+ 
                "Fr "+list_hours.get(4)+"; "+
                "Sa "+list_hours.get(5)+"; "+ 
                "Su "+list_hours.get(6));
        return hours;
    }
    public static void putAddressesList(Dictionary<String, String> tagDict, ImmutableList<SchemaOrgType> addresses){
        if (addresses.size() > 0) {
            if (!addresses.get(0).getFullTypeName().equals("http://schema.org/Text")) {
                PostalAddress address = (PostalAddress) addresses.get(0);
                putIfNotEmpty(tagDict, "addr:country", Extract(address.getAddressCountryList(), false));
                putIfNotEmpty(tagDict, "addr:city", Extract(address.getAddressLocalityList(), false));
                putIfNotEmpty(tagDict, "addr:street", Extract(address.getStreetAddressList(), false));
                putIfNotEmpty(tagDict, "addr:postcode", Extract(address.getPostalCodeList(), false));
            } else {
                putIfNotEmpty(tagDict, "addr:to_split", Extract(addresses, false));
            }
        }
    }
}
