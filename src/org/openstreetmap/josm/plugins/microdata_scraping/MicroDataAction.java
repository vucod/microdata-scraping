package org.openstreetmap.josm.plugins.microdata_scraping;

import static org.openstreetmap.josm.tools.I18n.tr;
import static org.openstreetmap.josm.tools.I18n.trn;

import java.io.StringWriter;
import java.io.PrintWriter;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.BooleanUtils;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.command.ChangeCommand;
import org.openstreetmap.josm.command.Command;
import org.openstreetmap.josm.command.SequenceCommand;
import org.openstreetmap.josm.data.UndoRedoHandler;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.Relation;
import org.openstreetmap.josm.data.osm.Way;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.Notification;
import org.openstreetmap.josm.tools.HttpClient;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

public class MicroDataAction extends JosmAction {

    public MicroDataAction() {
        super(tr("Scrape website"), new ImageProvider("icon.png"), tr("Scrape website"),
                Shortcut.registerShortcut("Scrape website", tr("Scrape website"),
                        KeyEvent.VK_A, Shortcut.CTRL_SHIFT), false, "scrapeWebsite",
                true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        // Get the currently selected object
        final Collection<OsmPrimitive> sel = MainApplication.getLayerManager().getEditDataSet().getSelected();

        if (sel.size() != 1) {
            new Notification(tr("Microdata scraper: Please select exactly one object."))
                    .setIcon(JOptionPane.ERROR_MESSAGE)
                    .show();

            return;
        }

        final List<Command> commands = new ArrayList<>();
        for (OsmPrimitive selectedObject : sel) {
            String website = selectedObject.get("website");
            String validWebsite = website;
        	if(website == null){
                String addrWebsite = selectedObject.get("addr:website");
        	    if(addrWebsite != null){
                    validWebsite = addrWebsite;
                } else {
                    new Notification(tr("No website provided")).setIcon(JOptionPane.ERROR_MESSAGE).show();
                    return;
                }
            }

            new Notification(tr("Trying to scrape: ")+validWebsite).setIcon(JOptionPane.INFORMATION_MESSAGE).show();
        	OsmPrimitive newObject = loadData(selectedObject, validWebsite);
        	if(newObject != null){
        		commands.add(new ChangeCommand(selectedObject, newObject));
        	}
        }
        if (!commands.isEmpty()) {
            UndoRedoHandler.getInstance().add(new SequenceCommand(trn("Add address", "Add addresses", commands.size()), commands));
        }
    }


    public static OsmPrimitive loadData(OsmPrimitive selectedObject, String website){
        String exception = null;
        String notifIntro = "<strong>" + tr("Micro data scraper") + "</strong><br /> :";
        String template = StringUtils.defaultIfEmpty(selectedObject.get("scraper_microdata:template"), "no_template");

        final OsmPrimitive newObject = selectedObject instanceof Node
                ? new Node(((Node) selectedObject))
                : selectedObject instanceof Way
                ? new Way((Way) selectedObject)
                : selectedObject instanceof Relation
                ? new Relation((Relation) selectedObject)
                : null;

        ScraperUtil scraperUtil = new ScraperUtil();
        Dictionary<String, String> tagDict = scraperUtil.scrap(website, template);
        exception = tagDict.get("Exception");
        tagDict.remove("Exception");

        for (Enumeration<String> e = tagDict.keys(); e.hasMoreElements();) {
            String key = e.nextElement();
            if (newObject.get(key) == null ){
                newObject.put(key, tagDict.get(key));
            } else {
                if (newObject.get("scraper_microdata:update") != null ){
                    if (newObject.get("scraper_microdata:update").equals("true")){
                        if (!newObject.get(key).equals(tagDict.get(key))){
                            newObject.put(key+":update", tagDict.get(key));
                        }
                    }
                }
            }
        }

        if (exception.equals("false")) {
            MainApplication.getLayerManager().getEditDataSet().addChangeSetTag("source", "scraper_microdata");
            new Notification(notifIntro + tr("Successfully added micro data to the selected object.") 
            ).setIcon(JOptionPane.INFORMATION_MESSAGE).setDuration(2000).show();
            return newObject;
        } else if (exception.equals("empty")){
            new Notification(notifIntro + tr("No micro data found.")).setIcon(JOptionPane.ERROR_MESSAGE).show();
        } else {
            new Notification(notifIntro + tr("An unexpected exception occurred: ") + exception
            ).setIcon(JOptionPane.ERROR_MESSAGE).show();
        }
        return null;
    }


    @Override
    protected void updateEnabledState() {
        if (getLayerManager().getEditDataSet() == null) {
            setEnabled(false);
        } else {
            updateEnabledState(getLayerManager().getEditDataSet().getSelected());
        }
    }

    @Override
    protected void updateEnabledState(final Collection<? extends OsmPrimitive> selection) {
        // Enable it only if exactly one object is selected.
        setEnabled(selection != null && selection.size() == 1);
    }

}
